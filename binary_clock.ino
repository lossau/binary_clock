#include <LiquidCrystal.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

const unsigned long clockInterval = 1000;
unsigned long clockTimer = 0;

long int h = 9;
long int m = 49;
long int s = 0;

void setup() {
  lcd.begin(16, 2);

  clockTimer = clockInterval;
}

long int convertDecimalToBinary(int decimalNumber) {
  long int binaryNumber = 0;
  long int remainder, i = 1;

  while (decimalNumber != 0) {
    remainder = decimalNumber % 2;
    decimalNumber /= 2;
    binaryNumber += remainder * i;
    i *= 10;
  }
  return binaryNumber;
}

void clockTick() {
  if (s == 60) {
    m++;
    s = 0;
  }

  if (m == 60) {
    h++;
    m = 0;
  }
}

void enableClock() {
  if ((millis() - clockTimer) >= clockInterval) {

    clockTick();

    // show on screen
    lcd.setCursor(0, 0);
    lcd.clear();

    char horas[10];
    char minutos[10];
    char segundos[10];

    sprintf(horas, "%04li", convertDecimalToBinary(h));
    sprintf(minutos, "%06li", convertDecimalToBinary(m));
    sprintf(segundos, "%06li", convertDecimalToBinary(s));

    lcd.print(horas);
    lcd.print(":");
    lcd.setCursor(0, 1);
    lcd.print(minutos);
    lcd.print(":");
    lcd.print(segundos);

    s++;

    clockTimer = millis();
  }
}

void loop() { enableClock(); }
